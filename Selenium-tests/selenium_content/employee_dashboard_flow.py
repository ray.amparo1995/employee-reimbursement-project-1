from selenium import webdriver
import time

from poms.employee_login_page import EmployeeLoginPage
from poms.employee_dashboard_page import EmployeeDashboardPage
from poms.employee_logout import EmployeeDashboardLogout

driver = webdriver.Chrome()

driver.get('http://localhost:5000/employee_login.html')

# Employee logs in
el_page = EmployeeLoginPage(driver)
el_page.enter_credentials()
el_page.click_login_button()

# Employee dashboard and submits request

driver.implicitly_wait(1)
ed_page = EmployeeDashboardPage(driver)
ed_page.enter_request()
ed_page.click_submit_request()

# Employee logs out
employee_logout = EmployeeDashboardLogout(driver)
employee_logout.click_logout()

# Quit once test is done
driver.quit()
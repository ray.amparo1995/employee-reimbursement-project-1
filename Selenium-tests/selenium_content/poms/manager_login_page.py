class ManagerLoginPage:
    email_input = 'floatingInput'
    password_input = 'floatingPassword'
    login_button = 'login-button'

    def __init__(self, driver):
        self.driver = driver

    def enter_credentials(self):
        self.driver.find_element_by_id(self.email_input).send_keys('magic.johnson@lakers.com')
        self.driver.find_element_by_id(self.password_input).send_keys('bestpg123')

    def click_login_button(self):
        self.driver.find_element_by_id(self.login_button).click()
class EmployeeDashboardPage:
    amount_input = 'amount'
    reason_input = 'reason'
    request_button = 'submitRequest'

    def __init__(self, driver):
        self.driver = driver

    def enter_request(self):
        self.driver.find_element_by_id(self.amount_input).send_keys(5000)
        self.driver.find_element_by_id(self.reason_input).send_keys('Off-season training')

    def click_submit_request(self):
        self.driver.find_element_by_id(self.request_button).click()
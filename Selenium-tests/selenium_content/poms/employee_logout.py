class EmployeeDashboardLogout:
    logout_button = 'logout'

    def __init__(self, driver):
        self.driver = driver

    def click_logout(self):
        self.driver.find_element_by_id(self.logout_button).click()
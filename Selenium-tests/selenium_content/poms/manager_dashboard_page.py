class ManagerDashboardPage:
    pending_button = 'pending'
    modal_review_button = '#modal0'
    optional_note = 'optional-note'
    deny_button = 'btn-danger'
    accept_button = 'btn-success'
    logout_button = 'logout'

    def __init__(self, driver):
        self.driver = driver

    def click_pending_tab(self):
        self.driver.find_element_by_id(self.pending_button).click()

    def click_first_pending_review(self):
        self.driver.find_element_by_id(self.modal_review_button).click()

    def enter_deny_note(self):
        self.driver.find_element_by_id(self.optional_note).send_keys('Denied')

    def enter_accept_note(self):
        self.driver.find_element_by_id(self.optional_note).send_keys('Accepted')

    def click_deny_request(self):
        self.driver.find_element_by_class_name(self.deny_button).click()

    def click_accept_request(self):
        self.driver.find_element_by_class_name(self.accept_button).click()

    def manager_logout(self):
        self.driver.find_element_by_id(self.logout_button).click()
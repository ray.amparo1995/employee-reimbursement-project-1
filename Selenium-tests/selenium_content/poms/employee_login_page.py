from selenium.webdriver.common.keys import Keys

class EmployeeLoginPage:
    email_input_id = 'floatingInput'
    password_input_id = 'floatingPassword'
    login_button_id = 'login-button'

    def __init__(self, driver):
        self.driver = driver

    def enter_credentials(self):
        self.driver.find_element_by_id(self.email_input_id).send_keys('dame@dolla.com')
        self.driver.find_element_by_id(self.password_input_id).send_keys('ineedaring')

    def click_login_button(self):
        self.driver.find_element_by_id(self.login_button_id).click()

from selenium import webdriver

from poms.manager_login_page import ManagerLoginPage
from poms.manager_dashboard_page import ManagerDashboardPage

driver = webdriver.Chrome()

driver.get('http://localhost:5000/manager_login.html')

# Manager logs in
ml_page = ManagerLoginPage(driver)
ml_page.enter_credentials()
ml_page.click_login_button()

# Manager clicks pending tab on manager dashboard
driver.implicitly_wait(1)
md_page = ManagerDashboardPage(driver)
md_page.click_pending_tab()

# Manager clicks on the first pending request and either denies or accepts it with an optional note
md_page.click_first_pending_review()

# Deny request
# md_page.enter_deny_note()
# md_page.click_deny_request()

# Accept request
md_page.enter_accept_note()
md_page.click_accept_request()

# Quit once test is done
driver.quit()

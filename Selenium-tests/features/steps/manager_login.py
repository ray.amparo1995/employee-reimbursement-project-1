from behave import *

@given('a manager is on the manager login page')
def test_on_manager_login_page(context):
    context.driver.get('http://localhost:5000/manager_login.html')

@given('a manager enters teh correct email and password')
def test_manager_enters_credentials(context):
    context.ml_page.enter_credentials()

@when('the manager pushes the login button')
def test_manager_clicks_login(context):
    context.ml_page.click_login_button()

@then('the manager is redirected to their management dashboard home page')
def test_manager_redirected_to_dashbaord(context):
    context.driver.get('http://localhost:5000/manager_dashboard.html')
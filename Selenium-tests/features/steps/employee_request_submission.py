from behave import *

@given('the employee is on their home tab of their reimbursement dashboard')
def test_on_employee_dashboard_page(context):
    context.driver.get('http://localhost:5000/employee_dashboard.html')

@given('the employee inputs an amount and reason for their reimbursement request')
def test_employee_enters_request_info(context):
    context.ed_page.enter_request()

@when('the employee presses the submit button')
def test_request_submit(context):
    context.ed_page.click_submit_request()

@then('the employee logs out')
def test_logout(context):
    context.employee_logout.click_logout()

from behave import *

@given('an employee is on the employee login page')
def test_on_employee_login_page(context):
    context.driver.get('http://localhost:5000/employee_login.html')

@given('an employee enters their correct email and password')
def test_employee_enters_credentials(context):
    context.el_page.enter_credentials()

@when('the employee pushes the login button')
def test_employee_clicks_login(context):
    context.el_page.click_login_button()

@then('the employee is redirected to their employee reimbursement dashboard home page')
def test_employee_redirected_to_dashboard(context):
    context.driver.get('http://localhost:5000/employee_dashboard.html')

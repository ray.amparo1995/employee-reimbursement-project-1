from behave import *

@given('a manager is on their management dashboard')
def test_on_manager_dashboard(context):
    context.driver.get('http://localhost:5000/manager_dashboard.html')

@given('clicks on the pending tab to route to review all of the employees pending reimbursement request')
def test_routes_to_pending_request(context):
    context.md_page.click_pending_tab()

@given('the manager pushes the review button for the top request')
def test_clicks_first_review(context):
    context.driver.implicitly_wait(1)
    context.md_page.click_first_pending_review()

@when('the manager inputs a note for the denial')
def test_denial_note(context):
    context.md_page.enter_deny_note()

@then('the manager pushes the deny button to deny the request')
def test_press_deny(context):
    context.md_page.click_deny_request()

@when('the manager inputs a note for the acceptance')
def test_acceptance_note(context):
    context.md_page.enter_accept_note()

@then('the manager pushes the accept button to accept the request')
def test_press_accept(context):
    context.md_page.click_accept_request()

Feature: Employee Reimbursement Submission

  Background: An employee is on their reimbursement dashboard after logging in with their email and password
    Given the employee is on their home tab of their reimbursement dashboard
    And the employee inputs an amount and reason for their reimbursement request

  Scenario: An employee is on the employee dashboard and would like to submit a new reimbursement request
    When the employee presses the submit button
    Then the employee logs out
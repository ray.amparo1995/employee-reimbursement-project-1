Feature: Employee Reimbursement Login

  Background: An employee is on the employee login page and enters the correct email and password
    Given an employee is on the employee login page
    And an employee enters their correct email and password

  Scenario: An employee is on the employee login page and would like to login with the correct credentials
    When the employee pushes the login button
    Then the employee is redirected to their employee reimbursement dashboard home page
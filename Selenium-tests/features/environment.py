from selenium import webdriver
from selenium_content.poms.employee_login_page import EmployeeLoginPage
from selenium_content.poms.employee_dashboard_page import EmployeeDashboardPage
from selenium_content.poms.employee_logout import EmployeeDashboardLogout
from selenium_content.poms.manager_login_page import ManagerLoginPage
from selenium_content.poms.manager_dashboard_page import ManagerDashboardPage

def before_all(context):
    context.driver = webdriver.Chrome()
    context.el_page = EmployeeLoginPage(context.driver)
    context.ed_page = EmployeeDashboardPage(context.driver)
    context.employee_logout = EmployeeDashboardLogout(context.driver)
    context.ml_page = ManagerLoginPage(context.driver)
    context.md_page = ManagerDashboardPage(context.driver)

def after_all(context):
    context.driver.quit()
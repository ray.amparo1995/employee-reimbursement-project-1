Feature: Manager Reviews Pending Requests

  Background: A manager is on their management dashboard and clicks on the pending tab to review all employees requests
    Given a manager is on their management dashboard
    And clicks on the pending tab to route to review all of the employees pending reimbursement request
    And the manager pushes the review button for the top request

  Scenario: A manager is on the pending reimbursement requests page and denies a request
    When the manager inputs a note for the denial
    Then the manager pushes the deny button to deny the request

  Scenario: A manager is on the pending reimbursement requests page and accepts a request
    When the manager inputs a note for the acceptance
    Then the manager pushes the accept button to accept the request
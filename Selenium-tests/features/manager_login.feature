Feature: Manager Reimbursement Login

  Background: A manager is on the manager login page and enters the correct email and password
    Given a manager is on the manager login page
    And a manager enters teh correct email and password

  Scenario: A manager is on the manager login page and would like to login with their correct credentials
    When the manager pushes the login button
    Then the manager is redirected to their management dashboard home page
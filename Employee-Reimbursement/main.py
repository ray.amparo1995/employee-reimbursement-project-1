from src.app import app
from src.controllers import employee_controllers
from src.controllers import reimbursements_controllers
from src.controllers import managers_controllers
from src.controllers import statistics_controllers

if __name__ == '__main__':
    app.run()
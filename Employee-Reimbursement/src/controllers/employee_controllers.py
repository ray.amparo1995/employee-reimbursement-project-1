from src.app import app
from flask import request, Response, redirect, render_template, make_response, jsonify
from json import dumps
from src.models.employee_model import EmployeeEncoder
import src.services.employee_services as employee_services
import logging

# Gets employee
@app.route('/employees/<int:employee_id>', methods=['GET'])
def get_employees(employee_id):
    employee = employee_services.get_employee(employee_id)
    if employee == {}:
        return Response('No such user exists', status=404)
    return dumps(employee, cls=EmployeeEncoder)

# Login for employees
@app.route('/employees/login', methods=['POST'])
def employee_login():
    req_body = request.get_json()
    email = req_body['email']
    password = req_body['password']
    employee_id = employee_services.get_employee_id(email, password)
    if employee_id == 'Wrong credentials or no such employee exists':
        return Response('Wrong credentials. Please log in again.', status=202)
    return dumps(employee_id, cls=EmployeeEncoder)
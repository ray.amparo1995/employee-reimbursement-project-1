from src.app import app
from json import dumps
from flask import request, Response
from src.models.employee_model import EmployeeEncoder
import src.services.reimbursement_services as reimbursements_services
import src.services.employee_services as employee_services
import src.dao.reimbursement_dao as reimbursement_dao
import logging

# Employee Reimbursement Routes

# Retrieving Employees Reimbursements Past or Pending
@app.route('/employees/<int:employee_id>/reimbursements', methods=['GET', 'POST'])
def past_or_present_reimbursements(employee_id):
    # GET
    # Past
    if request.method == 'GET':
        if employee_services.get_employee(employee_id) == {}:
            return Response('No such user exists', status=404)
        if request.args.get('past'):
            past_reimbursements = reimbursements_services.get_past_reimbursements(employee_id)
            if past_reimbursements == []:
                return Response('You have no past requests.', status=202)
            return dumps(past_reimbursements, cls=EmployeeEncoder)
        # Pending
        if request.args.get('pending'):
            pending_reimbursements = reimbursements_services.get_pending_reimbursements(employee_id)
            if pending_reimbursements == []:
                return Response('You have no pending requests.', status=202)
            return dumps(pending_reimbursements, cls=EmployeeEncoder)
    # POST
    if request.method == 'POST':
        if employee_services.get_employee(employee_id) == {}:
            return Response('No such user exists', status=404)
        # This would be a form once we create the frontend
        req_body = request.get_json()
        amount = req_body['amount']
        reason = req_body['reason']
        reimbursements_services.create_reimbursement_request(employee_id, amount, reason)
        return Response('Successfully submitted reimbursement request', status=200)

# Updates or deletes a pending request
@app.route('/employees/<int:employee_id>/reimbursements/<int:reimbursement_id>', methods=['PUT', 'DELETE', 'PATCH'])
def update_pending_request(employee_id, reimbursement_id):
    # PUT
    if request.method == 'PUT':
        if employee_services.get_employee(employee_id) == {}:
            return Response('No such user exists', status=404)
        if reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id) == [] or reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id)[0][6] != 'Pending':
            return Response('Employee does not have a reimbursement request with that number or the request has been resolved', status=404)
        # This will be request.form.get() once frontend is built
        req_body = request.get_json()
        amount = req_body['amount']
        reason = req_body['reason']
        reimbursements_services.update_pending_request(employee_id, reimbursement_id, amount, reason)
        return Response('Successfully update reimbursement request', status=200)
    # DELETE
    if request.method == 'DELETE':
        if employee_services.get_employee(employee_id) == {}:
            return Response('No such user exists', status=404)
        if reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id) == [] or reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id)[0][6] != 'Pending':
            return Response('Employee does not have a reimbursement request with that number or the request has been resolved', status=404)
        reimbursements_services.delete_pending_request(employee_id, reimbursement_id)
        return Response('Successfully deleted pending reimbursement request', status=200)
    # PATCH
    # ONLY MANAGERS CAN ACCESS
    if request.method == 'PATCH':
        if employee_services.get_employee(employee_id) == {}:
            return Response('No such user exists', status=404)
        if reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id) == [] or reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id)[0][6] != 'Pending':
            return Response('Employee does not have a reimbursement request with that number or the request has been resolved', status=404)
        # This might be a form once the frontend is built
        req_body = request.get_json()
        deny_or_accept = reimbursements_services.deny_or_accept_request(employee_id, reimbursement_id, req_body)
        if deny_or_accept == 'Successfully denied reimbursement requests':
            return Response('Request has been denied', status=200)
        if deny_or_accept == 'Successfully accepted request':
            return Response('Request has been accepted', status=200)

# Manager Reimbursement Routes

# Gets reimbursement requests from the past 7 days
@app.route('/employees/reimbursements/recent', methods=['GET'])
def requests_past_seven_days():
    recent_requests = reimbursements_services.get_recent_requests()
    if recent_requests == []:
        return Response('No recent reimbursement requests', status=202)
    return dumps(recent_requests, cls=EmployeeEncoder)

# Gets all reimbursements from the past
@app.route('/employees', methods=['GET'])
def get_employees_requests():
    if request.args.get('reimbursementsPast'):
        past_requests = reimbursements_services.get_all_past_reimbursements()
        if past_requests == []:
            return Response('No past requests', status=202)
        return dumps(past_requests, cls=EmployeeEncoder)
    if request.args.get('reimbursementsPending'):
        pending_requests = reimbursements_services.get_all_pending_reimbursements()
        if pending_requests == []:
            return Response('No pending requests', status=202)
        return dumps(pending_requests, cls=EmployeeEncoder)
    all_employees = employee_services.get_all_employees()
    return dumps(all_employees, cls=EmployeeEncoder)
from src.app import app
from json import dumps
from flask import request, Response
import src.services.manager_services as manager_services
from src.models.manager_model import ManagerEncoder
import logging

# Gets manager's information
@app.route('/managers/<int:manager_id>', methods=['GET'])
def get_manager(manager_id):
    manager = manager_services.get_manager(manager_id)
    if manager == {}:
        return Response('No such manager exists', status=404)
    return dumps(manager, cls=ManagerEncoder)

# Gets manager via email and password
@app.route('/managers/login', methods=['POST'])
def manager_login():
    req_body = request.get_json()
    email = req_body['email']
    password = req_body['password']
    manager_id = manager_services.get_manager_id(email, password)
    if manager_id == 'Wrong credentials or no such manager exists':
        return Response('Wrong credentials. Please log in again.', status=202)
    return dumps(manager_id, cls=ManagerEncoder)
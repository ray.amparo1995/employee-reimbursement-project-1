from src.app import app
from json import dumps
from flask import request, Response
import src.services.statistic_services as statistic_services
from src.models.employee_model import EmployeeEncoder

# Gets overall statistics for company (all employees)
@app.route('/statistics', methods=['GET'])
def get_company_statistics():
    company_statistics = statistic_services.get_company_statistics()
    if company_statistics == []:
        return Response('No company statistics', status=202)
    return dumps(company_statistics, cls=EmployeeEncoder)
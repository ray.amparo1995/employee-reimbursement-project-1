Project 1 User Stories

Employees
1.) As a user employee, I am able to log into my account and am prompted with my Information and am prompted with a submission form for new reimbursement requests I'd like to make.
2.) As a user employee, I can filter my reimbursements whether they are past or pending.
3.) As a user employee, I can post a new reimbursement request with an amount and reason for my filing.
4.) As a user employee, I can logout of my reimbursement dashboard and redirected to the login page.

Managers
1.) As a user manager, I am able to log into my account and view all of my employees’ reimbursements, past and pending.
2.) As a user manager, I am prompted with recent reimbursement requests of the past 7 days along with my company information as my home page.
3.) As a manager, I am able to filter the reimbursements I see on my page whether past or pending.
4.) As a manager, I am able to either accept or deny any pending reimbursement requests and place an optional note if I would like.
5.) As a manager, I have access to the statistics page, where I am able to see which employee spends the most, the employee who spends the least, the mean of both all individual employees and all of the employees together and the median of individual employees and all of the employees together and the range of both all individual employees and all of the employees together.
6.) As a manger, I am able to logout of my management dashboard and am redirected back to the manager login page.

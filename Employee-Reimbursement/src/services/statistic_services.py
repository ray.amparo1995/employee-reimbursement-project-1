import src.dao.statistic_dao as statistic_dao
from src.models.statistics_model import Statistic
from re import sub
from decimal import Decimal
import src.dao.reimbursement_dao as reimbursement_dao
from statistics import median
import logging

# Logs
logging.basicConfig(filename='statistic_services.log', level=logging.INFO)

def convert_statistics(statistics_row):
    statistic_list = []
    count = 0
    if len(statistics_row) > 0:
        for statistic in statistics_row:
            statistic_list.insert(count, Statistic(statistic[0], statistic[1], statistic[2], statistic[3], statistic[4], statistic[5], statistic[6]))
            count + 1
    return statistic_list

def get_employee_statistics(employee_id):
    employee_statistics = statistic_dao.get_employee_statistics(employee_id)
    return convert_statistics(employee_statistics)

def create_statistics(employee_id, pending_amount):
    statistic_dao.create_statistics(employee_id, pending_amount)
    return 'Successfully created statistics for employee'

def update_pending_statistics(employee_id, amount_requested):
    current_statistics = statistic_dao.get_employee_statistics(employee_id)
    logging.info(float(Decimal(sub(r'[^\d.]', '', current_statistics[0][1]))))
    current_overall_requested_amount = float(Decimal(sub(r'[^\d.]', '', current_statistics[0][1])))
    new_overall_requested_amount = current_overall_requested_amount + amount_requested
    statistic_dao.update_overall_requested_statistics(employee_id, new_overall_requested_amount)
    return 'Successfully updated pending statistics (Overall requested amount)'

# Gets company statistics
def get_company_statistics():
    list_amounts = []
    overall_requested = 0.00
    accepted = 0.00
    denied = 0.00
    mean = 0.00
    median_amount = 0.00
    range = 0.00
    accepted_requests = reimbursement_dao.get_all_accepted_reimbursements()
    denied_requests = reimbursement_dao.get_all_denied_reimbursements()
    all_requests = reimbursement_dao.get_all_reimbursement_requests()
    if accepted_requests == []:
        return list_amounts
    # Accepted
    for request in accepted_requests:
        float_amount = float(Decimal(sub(r'[^\d.]', '', request[2])))
        accepted = accepted + float_amount
        mean = mean + float_amount
        list_amounts.insert(0, float_amount)
    # Denied
    for request in denied_requests:
        float_amount = float(Decimal(sub(r'[^\d.]', '', request[2])))
        denied = denied + float_amount
    # Overall Requested
    for request in all_requests:
        float_amount = float(Decimal(sub(r'[^\d.]', '', request[2])))
        overall_requested = overall_requested + float_amount
    # Mean
    mean = mean / len(list_amounts)
    mean = round(mean, 2)
    sorted_amounts = sorted(list_amounts)
    # Median
    median_amount = median_amount + median(sorted_amounts)
    # Range
    minimum = min(list_amounts)
    maximum = max(list_amounts)
    range = range + (maximum - minimum)
    return Statistic(0, overall_requested, accepted, denied, mean, median_amount, range)
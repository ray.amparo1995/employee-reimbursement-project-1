import src.dao.employee_dao as employee_dao
import src.services.reimbursement_services as reimbursement_services
import src.services.statistic_services as statistic_services
from src.models.employee_model import Employee
import src.dao.reimbursement_dao as reimbursement_dao
import logging

# Logs
logging.basicConfig(filename='employee_service.log', level=logging.INFO)

# Gets employee via ID
def get_employee(employee_id):
    employee_dict = {}
    employee = employee_dao.get_employee(employee_id)
    if employee == []:
        return {}
    reimbursement_list = reimbursement_services.get_employee_reimbursement(employee_id)
    statistic_list = statistic_services.get_employee_statistics(employee_id)
    # Logs ensuring the data I'm getting is correct
    logging.info(employee)
    logging.info(reimbursement_list)
    logging.info(statistic_list)
    employee_dict['1'] = Employee(employee[0][0], employee[0][1], employee[0][2], employee[0][3], reimbursement_list, statistic_list)
    return employee_dict

# Gets all employees with all of their reimbursements
def get_all_employees():
    employee_dict = {}
    employees = employee_dao.get_all_employees()
    for employee in employees:
        reimbursement_rows = reimbursement_dao.get_employee_reimbursement(employee[0])
        reimbursement_list = reimbursement_services.convert_reimbursement_rows(reimbursement_rows)
        statistic_rows = statistic_services.get_employee_statistics(employee[0])
        employee_dict[employee[0]] = Employee(employee[0], employee[1], employee[2], employee[3], reimbursement_list, statistic_rows)
    return employee_dict

# Gets employee ID via email and password
def get_employee_id(email, password):
    employee_id_list = employee_dao.get_employee_id(email, password)
    if employee_id_list == []:
        return 'Wrong credentials or no such employee exists'
    employee_id = employee_id_list[0][0]
    return get_employee(employee_id)
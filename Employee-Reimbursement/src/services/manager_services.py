from src.models.manager_model import Manager
import src.dao.manager_dao as manager_dao
import logging

def get_manager(manager_id):
    manager_dict = {}
    manager = manager_dao.get_manager(manager_id)
    if manager == []:
        return manager_dict
    manager_dict['Manager'] = Manager(manager[0][0], manager[0][1], manager[0][2], manager[0][3])
    return manager_dict

# Gets manager ID via email and password
def get_manager_id(email, password):
    manager_id_list = manager_dao.get_manager_id(email, password)
    if manager_id_list == []:
        return 'Wrong credentials or no such manager exists'
    manager_id = manager_id_list[0][0]
    return get_manager(manager_id)
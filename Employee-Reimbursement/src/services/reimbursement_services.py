import src.dao.reimbursement_dao as reimbursement_dao
from src.models.reimbursement_model import Reimbursement
from datetime import datetime, date
import src.services.statistic_services as statistic_services
import src.dao.statistic_dao as statistic_dao
import src.dao.employee_dao as employee_dao
from re import sub
from decimal import Decimal
from statistics import median
import logging

# Logs
logging.basicConfig(filename='reimbursement_services.log', level=logging.INFO)

# Converts reimbursements into a list
def convert_reimbursement_rows(reimbursement_rows):
    reimbursement_list = []
    count = 0
    if len(reimbursement_rows) > 0:
        for reimbursement in reimbursement_rows:
            # Data below is coming in as the type of date
            logging.info(reimbursement[4])
            reimbursement_list.insert(count, Reimbursement(reimbursement[0], reimbursement[1], reimbursement[2], reimbursement[3], str(reimbursement[4]), reimbursement[5], reimbursement[6], reimbursement_note=reimbursement[7]))
            count + 1
        return reimbursement_list

# Grabs all reimbursements for employee
def get_employee_reimbursement(employee_id):
    employee_reimbursements = reimbursement_dao.get_employee_reimbursement(employee_id)
    return convert_reimbursement_rows(employee_reimbursements)

# Gets past reimbursements for employee
def get_past_reimbursements(employee_id):
    past_reimbursements = reimbursement_dao.get_past_reimbursements(employee_id)
    return convert_reimbursement_rows(past_reimbursements)

# Gets pending reimbursements for employee
def get_pending_reimbursements(employee_id):
    pending_reimbursements = reimbursement_dao.get_pending_reimbursements(employee_id)
    return convert_reimbursement_rows(pending_reimbursements)

# Create new reimbursement request for employee
def create_reimbursement_request(employee_id, amount_requested, reason):
    employee_statistics = statistic_dao.get_employee_statistics(employee_id)
    logging.info(employee_statistics)
    if len(employee_statistics) < 1:
        statistic_services.create_statistics(employee_id, amount_requested)
    else:
        statistic_services.update_pending_statistics(employee_id, amount_requested)
    date_requested = datetime.today().date()
    reimbursement_dao.create_reimbursement_request(employee_id, amount_requested, reason, date_requested)
    return 'Successfully submitted reimbursement request'

# Update a pending request
def update_pending_request(employee_id, reimbursement_id, amount, reason):
    updated_request = reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id)
    current_amount = float(Decimal(sub(r'[^\d.]', '', updated_request[0][2])))
    employee_statistics = statistic_dao.get_employee_statistics(employee_id)
    current_overall_requested = float(Decimal(sub(r'[^\d.]', '', employee_statistics[0][1])))
    new_overall_requested = (current_overall_requested - current_amount) + amount
    statistic_dao.update_overall_requested_statistics(employee_id, new_overall_requested)
    reimbursement_dao.update_pending_request(employee_id, reimbursement_id, amount, reason)
    return 'Successfully updated reimbursement request'

# Deletes a pending request
def delete_pending_request(employee_id, reimbursement_id):
    updated_request = reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id)
    current_amount = float(Decimal(sub(r'[^\d.]', '', updated_request[0][2])))
    employee_statistics = statistic_dao.get_employee_statistics(employee_id)
    current_overall_requested = float(Decimal(sub(r'[^\d.]', '', employee_statistics[0][1])))
    new_overall_requested = (current_overall_requested - current_amount)
    statistic_dao.update_overall_requested_statistics(employee_id, new_overall_requested)
    reimbursement_dao.delete_pending_request(employee_id, reimbursement_id)
    return 'Successfully deleted pending reimbursement request'

# Gets recent request for the past seven days
def get_recent_requests():
    requests = []
    count = 0
    recent_requests = reimbursement_dao.get_recent_requests()
    if recent_requests == []:
        return requests
    for request in recent_requests:
        owner_id = request[0]
        employee_name = employee_dao.get_employee_name(owner_id)
        requests.insert(count, Reimbursement(request[0], request[1], request[2], request[3], str(request[4]), request[5], request[6], employee_name[0][0], request[7]))
        count + 1
    return requests

# Gets all past reimbursements for all employees
def get_all_past_reimbursements():
    past_requests = []
    count = 0
    past_reimbursements = reimbursement_dao.get_all_past_reimbursements()
    if past_reimbursements == []:
        return past_requests
    for request in past_reimbursements:
        owner_id = request[0]
        employee_name = employee_dao.get_employee_name(owner_id)
        past_requests.insert(count, Reimbursement(request[0], request[1], request[2], request[3], str(request[4]), request[5], request[6], employee_name[0][0], request[7]))
        count + 1
    return past_requests

# Gets all pending reimbursements for all employees
def get_all_pending_reimbursements():
    pending_requests = []
    count = 0
    pending_reimbursements = reimbursement_dao.get_all_pending_reimbursements()
    if pending_reimbursements == []:
        return pending_requests
    for request in pending_reimbursements:
        owner_id = request[0]
        employee_name = employee_dao.get_employee_name(owner_id)
        pending_requests.insert(count, Reimbursement(request[0], request[1], request[2], request[3], str(request[4]), request[5], request[6], employee_name[0][0], request[7]))
        count + 1
    return pending_requests

# Manager denies or accept a pending employee request
def deny_or_accept_request(employee_id, reimbursement_id, req_body):
    if req_body['denied']:
        pending_reimbursement = reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id)
        float_denied_amount = float(Decimal(sub(r'[^\d.]', '', pending_reimbursement[0][2])))
        # employee_statistics = statistic_dao.get_employee_statistics(employee_id)
        # employee_denied_amount = float(Decimal(sub(r'[^\d.]', '', employee_statistics[0][3])))
        employee_denied_amount = 0.00
        denied_requests = reimbursement_dao.get_employee_denied_requests(employee_id)
        for request in denied_requests:
            float_amount = float(Decimal(sub(r'[^\d.]', '', request[2])))
            employee_denied_amount = employee_denied_amount + float_amount
        new_denied_amount = employee_denied_amount + float_denied_amount
        reimbursement_dao.deny_pending_request(employee_id, reimbursement_id, req_body['note'])
        statistic_dao.update_statistics_denied(employee_id, new_denied_amount)
        return 'Successfully denied reimbursement requests'
    if req_body['accepted']:
        pending_reimbursement = reimbursement_dao.get_specific_reimbursement(employee_id, reimbursement_id)
        float_accepted_amount = float(Decimal(sub(r'[^\d.]', '', pending_reimbursement[0][2])))
        # employee_statistics = statistic_dao.get_employee_statistics(employee_id)
        # employee_accepted_amount = float(Decimal(sub(r'[^\d.]', '', employee_statistics[0][2])))
        employee_accepted_amount = 0.00
        accepted_requests = reimbursement_dao.get_employee_accepted_requests(employee_id)
        for request in accepted_requests:
            float_amount = float(Decimal(sub(r'[^\d.]', '', request[2])))
            employee_accepted_amount = employee_accepted_amount + float_amount
        new_accepted_amount = employee_accepted_amount + float_accepted_amount
        # logging.info(new_accepted_amount)
        reimbursement_dao.accept_pending_request(employee_id, reimbursement_id, req_body['note'])
        employee_accepted_requests = reimbursement_dao.get_employee_accepted_requests(employee_id)
        logging.info(employee_accepted_requests)
        if len(employee_accepted_requests) > 1:
            list_amounts = []
            # overall_accepted = new_accepted_amount
            new_mean = 0.00
            new_median = 0.00
            new_range = 0.00
            accepted_requests = reimbursement_dao.get_employee_accepted_requests(employee_id)
            for amount in accepted_requests:
                float_amount = float(Decimal(sub(r'[^\d.]', '', amount[2])))
                new_mean = new_mean + float_amount
                list_amounts.insert(0, float_amount)
            # Mean
            new_mean = new_mean / len(list_amounts)
            new_mean = round(new_mean, 2)
            # Median
            sorted_amounts = sorted(list_amounts)
            new_median = new_median + median(sorted_amounts)
            # Range
            minimum = min(list_amounts)
            maximum = max(list_amounts)
            new_range = new_range + (maximum - minimum)
            statistic_dao.update_statistics_accepted(employee_id, new_accepted_amount, new_mean, new_median, new_range)
            return 'Successfully accepted request'
        mean_amount = new_accepted_amount
        median_amount = new_accepted_amount
        range_amount = new_accepted_amount
        statistic_dao.update_statistics_accepted(new_accepted_amount, mean_amount, median_amount, range_amount, employee_id)
        return 'Successfully accepted request'
create table employees(
	employee_id bigserial not null,
	employee_email varchar(40) unique not null,
	employee_password varchar(20) not null,
	employee_name varchar(20) not null,
	primary key (employee_id)
);

create table reimbursements(
	owner_id bigserial references employees(employee_id) not null,
	reimbursement_id bigserial not null,
	reimbursement_amount money not null,
	reimbursement_reason varchar(50) not null,
	reimbursement_date date not null,
	reimbursement_resolved boolean not null,
	reimbursement_accepted varchar(20) not null,
	primary key (reimbursement_id)
);
-- Forgot to add optional notes for the requests that are denied or accepted
alter table reimbursements add reimbursement_note varchar(70);

create table statistics(
	owner_id bigserial references employees(employee_id) unique not null,
	statistic_overall_amount_requested money not null,
	statistic_accepted_amount money not null,
	statistic_denied_amount money not null,
	statistic_mean money not null,
	statistic_median money not null,
	statistic_range money not null
);

create table managers(
	manager_id bigserial not null,
	manager_name varchar(20) not null,
	manager_email varchar(40) unique not null,
	manager_password varchar(20) not null,
	primary key (manager_id)
);

insert into employees values (default, 'john.doe@gmail.com', 'johndoerules', 'John Doe');
insert into employees values (default, 'jimmy.john@gmail.com', 'jimmyjohn123', 'Jimmy John');
insert into employees values (default, 'karen.carter@gmail.com', 'karencart', 'Karen Carter');
insert into employees values( default, 'suzy.clues@gmail.com', 'suzclues', ' Suzy Clues');
insert into employees values (default, 'cameron.payne@suns.com', 'campay', 'Cameron Payne');
insert into employees values (default, 'devin.booker@suns.com', 'sunsin5', 'Devin Booker');
insert into employees values (default, 'lbj@lakers.com', 'iamking', 'LeBron James');
insert into employees values (default, 'slim@reaper.net', 'slimreaper', 'Kevin Durant');

insert into reimbursements values (1, default, 2500.00, 'Business travel', '2021-05-23', false, 'Pending');
insert into reimbursements values (2, default, 1100.00, 'Business Laptop', '2021-01-18', true, 'Accepted');
insert into reimbursements values (3, default, 500.00, 'Business Desk', '2021-03-04', false, 'Pending');
insert into reimbursements values (4, default, 300.00, 'Certifications', '2021-04-26', false, 'Pending');
insert into reimbursements values (5, default, 1500.00, 'Business travel', '2021-02-07', true, 'Denied');
insert into reimbursements values (6, default, 500.00, 'Certifications', '2021-05-10', false, 'Pending');
insert into reimbursements values (7, default, 100.00, 'Certifications', '2021-05-14', false, 'Pending');
insert into reimbursements values (7, default, 5000.00, 'NBA Flights', '2021-03-14', true, 'Accepted');
insert into reimbursements values (7, default, 10000.00, 'Winning MVP', '2021-04-10', true, 'Denied');
insert into reimbursements values (7, default, 4000.00, 'Winning Rings', '2020-04-04', true, 'Accepted');

insert into statistics values(1, 2500.00, 0.00, 0.00, 0.00, 0.00, 0.00);
insert into statistics values(2, 1100.00, 1100.00, 0.00, 1100.00, 1100.00, 1100.00);
insert into statistics values(3, 500.00, 0.00, 0.00, 0.00, 0.00, 0.00);
insert into statistics values(4, 300.00, 0.00, 0.00, 0.00, 0.00, 0.00);
insert into statistics values(5, 1500.00, 0.00, 1500.00, 0.00, 0.00, 0.00);
insert into statistics values(6, 500.00, 0.00, 0.00, 0.00, 0.00, 0.00);
insert into statistics values(7, 19100.00, 9000.00, 10000.00, 4500.00, 4500.00, 1000.00);

insert into managers values (default, 'Earvin Johnson', 'magic.johnson@lakers.com', 'bestpg123');
insert into managers values (default, 'Jason Kidd', 'jason.kidd@mavericks.com', '10pieceanight');
insert into managers values (default, 'Larry Ayton', 'larry.ayton@gmail.com', 'larryayt321');

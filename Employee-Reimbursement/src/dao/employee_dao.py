from src.utils.db import db_connection

# Gets employees
def get_employee(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select * from employees where employee_id = %s', (employee_id,))
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets name of employee
def get_employee_name(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select employee_name from employees where employee_id = %s', (employee_id,))
        query_row = cur.fetchall()
        return query_row
    finally:
        con.close()

# Gets all employees
def get_all_employees():
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute('select * from employees')
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets employee via email and password
def get_employee_id(email, password):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute('select employee_id from employees where (employee_email = %s and employee_password = %s)', (email, password))
        query_row = cur.fetchall()
        return query_row
    finally:
        con.close()
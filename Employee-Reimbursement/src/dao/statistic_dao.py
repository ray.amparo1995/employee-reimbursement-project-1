from src.utils.db import db_connection
import logging

# Logs
logging.basicConfig(filename='statistic_dao.log', level=logging.DEBUG)

# Gets statistic for an employee
def get_employee_statistics(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select * from statistics where owner_id = %s', (employee_id,))
        query_row = cur.fetchall()
        return query_row
    finally:
        con.close()

# Creates a statistic for a user that has sent in their first reimbursement request
def create_statistics(employee_id, pending_amount):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'insert into statistics values (%s, %s, 0.00, 0.00, 0.00, 0.00, 0.00)', (employee_id, pending_amount))
        con.commit()
    finally:
        con.close()

# Updates employee's statistics
def update_overall_requested_statistics(employee_id, overall_requested):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'update statistics set statistic_overall_amount_requested = %s where owner_id = %s', (overall_requested, employee_id))
        con.commit()
    finally:
        con.close()

# Update statistics with denied request
def update_statistics_denied(employee_id, new_amount_denied):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"update statistics set statistic_denied_amount = %s where owner_id = %s", (new_amount_denied, employee_id))
        con.commit()
    finally:
        con.close()

# Update statistics with accepted request
def update_statistics_accepted(employee_id, new_amount_accepted, new_mean, new_median, new_range):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"update statistics set statistic_accepted_amount = %s, statistic_mean = %s, statistic_median = %s, statistic_range = %s where owner_id = %s", (new_amount_accepted, new_mean, new_median, new_range, employee_id))
        con.commit()
    finally:
        con.close()
from src.utils.db import db_connection
import logging

logging.basicConfig(filename='reimbursement_dao.log', level=logging.DEBUG)
def get_employee_reimbursement(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select * from reimbursements where owner_id = %s', (employee_id,))
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets past reimbursements for employee
def get_past_reimbursements(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select * from reimbursements where (owner_id = %s and reimbursement_resolved = true)', (employee_id,))
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets pending reimbursements for employee
def get_pending_reimbursements(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select * from reimbursements where (owner_id = %s and reimbursement_resolved = false)', (employee_id,))
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Creates new reimbursement request
def create_reimbursement_request(employee_id, amount, reason, date):
    try:
        con = db_connection()
        cur = con.cursor()
        # Had trouble with inserting and had to log this execution
        logging.debug(cur.execute(f"""insert into reimbursements values (%s, default, 
                                %s, %s, %s, false, 'Pending', '')""", (employee_id, amount, reason, date)))
        con.commit()
    finally:
        con.close()

# Get one specific reimbursement request
def get_specific_reimbursement(employee_id, reimbursement_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select * from reimbursements where (owner_id = %s and reimbursement_id = %s)', (employee_id, reimbursement_id))
        query_row = cur.fetchall()
        return query_row
    finally:
        con.close()

# Update a pending request
def update_pending_request(employee_id, reimbursement_id, amount, reason):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'update reimbursements set reimbursement_amount = %s, reimbursement_reason = %s where (owner_id = %s and reimbursement_id = %s)', (amount, reason, employee_id, reimbursement_id))
        con.commit()
    finally:
        con.close()

# Deletes a pending request
def delete_pending_request(employee_id, reimbursement_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'delete from reimbursements where (owner_id = %s and reimbursement_id = %s)', (employee_id, reimbursement_id))
        con.commit()
    finally:
        con.close()

# Gets recent reimbursement requests for the past 7 days
def get_recent_requests():
    try:
        con = db_connection()
        cur = con.cursor()
        # logging.debug(cur.execute("select * from reimbursements where (reimbursement_accepted = 'Pending' and reimbursement_resolved = false and reimbursement_date >= DATE(NOW()) + INTERVAL '-7 DAY' and reimbursement_date < DATE(NOW()) + INTERVAL '0 DAY')"))
        cur.execute("select * from reimbursements where (reimbursement_accepted = 'Pending' and reimbursement_resolved = false and reimbursement_date >= DATE(NOW()) + INTERVAL '-7 DAY' and reimbursement_date < DATE(NOW()) + INTERVAL '1 DAY')")
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets all past reimbursements for all employees
def get_all_past_reimbursements():
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute('select * from reimbursements where reimbursement_resolved = true')
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets all pending reimbursements for all employees
def get_all_pending_reimbursements():
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute("select * from reimbursements where reimbursement_resolved = false and reimbursement_accepted = 'Pending'")
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Get all accepted reimbursements
def get_all_accepted_reimbursements():
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute("select * from reimbursements where (reimbursement_resolved = true and reimbursement_accepted = 'Accepted')")
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Deny pending reimbursement request
def deny_pending_request(employee_id, reimbursement_id, note):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"update reimbursements set reimbursement_resolved = true, reimbursement_accepted = 'Denied', reimbursement_note = %s where (owner_id = %s and reimbursement_id = %s)", (note, employee_id, reimbursement_id))
        con.commit()
    finally:
        con.close()

# Accept pending reimbursement request
def accept_pending_request(employee_id, reimbursement_id, note):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"update reimbursements set reimbursement_resolved = true, reimbursement_accepted = 'Accepted', reimbursement_note = %s where (owner_id = %s and reimbursement_id = %s)", (note, employee_id, reimbursement_id))
        con.commit()
    finally:
        con.close()

# Gets all accepted requests for an employee
def get_employee_accepted_requests(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"select * from reimbursements where (owner_id = %s and reimbursement_resolved = true and reimbursement_accepted = 'Accepted')", (employee_id,))
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets all denied requests for an employee
def get_employee_denied_requests(employee_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"select * from reimbursements where (owner_id = %s and reimbursement_resolved = true and reimbursement_accepted = 'Denied')", (employee_id,))
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Gets all denied reimbursements for the company
def get_all_denied_reimbursements():
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"select * from reimbursements where (reimbursement_resolved = true and reimbursement_accepted = 'Denied')")
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()

# Get all reimbursement requests
def get_all_reimbursement_requests():
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f"select * from reimbursements")
        query_rows = cur.fetchall()
        return query_rows
    finally:
        con.close()
from src.utils.db import db_connection
import logging

# Gets single manager
def get_manager(manager_id):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select * from managers where manager_id = %s', (manager_id,))
        query_row = cur.fetchall()
        return query_row
    finally:
        con.close()

# Gets manager ID
def get_manager_id(email, password):
    try:
        con = db_connection()
        cur = con.cursor()
        cur.execute(f'select manager_id from managers where (manager_email = %s and manager_password = %s)', (email, password))
        query_row = cur.fetchall()
        return query_row
    finally:
        con.close()
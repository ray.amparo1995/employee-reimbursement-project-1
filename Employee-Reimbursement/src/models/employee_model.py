# For Encoder
import datetime

from src.models.reimbursement_model import Reimbursement
from src.models.statistics_model import Statistic
from json import JSONEncoder

class Employee:

    def __init__(self, employee_id, employee_email, employee_password, employee_name, employee_reimbursements_list, employee_statistics_list):
        self._employee_id = employee_id
        self._employee_email = employee_email
        self._employee_password = employee_password
        self._employee_name = employee_name
        self._employee_reimbursements_list = employee_reimbursements_list
        self._employee_statistics_list = employee_statistics_list

class EmployeeEncoder(JSONEncoder):
    def default(self, class_obj):
        if isinstance(class_obj, Employee):
            return class_obj.__dict__
        elif isinstance(class_obj, Reimbursement):
            return class_obj.__dict__
        elif isinstance(class_obj, Statistic):
            return class_obj.__dict__
        else:
            return super().default(self, class_obj)
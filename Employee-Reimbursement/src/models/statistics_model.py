class Statistic:

    def __init__(self, statistic_owner_id, statistic_overall_amount_requested, statistic_accepted_amount, statistic_denied_amount, statistic_mean, statistic_median, statistic_range):
        self._statistic_owner_id = statistic_owner_id
        self._statistic_overall_amount_request = statistic_overall_amount_requested
        self._statistic_accepted_amount = statistic_accepted_amount
        self._statistic_denied_amount = statistic_denied_amount
        self._statistic_mean = statistic_mean
        self._statistic_median = statistic_median
        self._statistic_range = statistic_range

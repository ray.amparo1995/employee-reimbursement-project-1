class Reimbursement:

    def __init__(self, owner_id, reimbursement_id, reimbursement_amount, reimbursement_reason, reimbursement_date, reimbursement_resolved, reimbursement_accepted, reimbursement_owner_name=None, reimbursement_note=''):
        self._owner_id = owner_id
        self._reimbursement_id = reimbursement_id
        self._reimbursement_amount = reimbursement_amount
        self._reimbursement_reason = reimbursement_reason
        self._reimbursement_date = reimbursement_date
        self._reimbursement_resolved = reimbursement_resolved
        self._reimbursement_accepted = reimbursement_accepted
        self._reimbursement_owner_name = reimbursement_owner_name
        self._reimbursement_note = reimbursement_note
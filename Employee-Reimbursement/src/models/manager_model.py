from json import JSONEncoder

class Manager:

    def __init__(self, manager_id, manager_name, manager_email, manager_password):
        self._manager_id = manager_id
        self._manager_name = manager_name
        self._manager_email = manager_email
        self._manager_password = manager_password

class ManagerEncoder(JSONEncoder):
    def default(self, class_obj):
        if isinstance(class_obj, Manager):
            return class_obj.__dict__
        else:
            return super().default(self, class_obj)
let home = document.querySelector('#home')
let pending = document.querySelector('#pending')
let past = document.querySelector('#past')
let logout = document.querySelector('#logout')

// Home click
home.addEventListener('click', function() {
    window.location = 'http://localhost:5000/employee_dashboard.html'
})

// Pending click
pending.addEventListener('click', function() {
    window.location = 'http://localhost:5000/employee_pending.html'
})

// Past click
past.addEventListener('click', function() {
    window.location = 'http://localhost:5000/employee_past.html'
})

// Logout click
logout.addEventListener('click', function() {
    window.location = 'http://localhost:5000/employee_login.html'
})
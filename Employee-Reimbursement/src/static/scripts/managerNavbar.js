let home = document.querySelector('#home')
let pending = document.querySelector('#pending')
let statistics = document.querySelector('#statistics')
let logout = document.querySelector('#logout')

// Home click
home.addEventListener('click', function() {
    window.location = 'http://localhost:5000/manager_dashboard.html'
})

// Pending click
pending.addEventListener('click', function() {
    window.location = 'http://localhost:5000/manager_pending.html'
})

//// Statistics click
statistics.addEventListener('click', function() {
    window.location = 'http://localhost:5000/statistics.html'
})

// Logout click
logout.addEventListener('click', function() {
    window.location = 'http://localhost:5000/manager_login.html'
})
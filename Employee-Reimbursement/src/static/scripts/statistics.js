// Variables for Company Stats
let companyAcceptedAmount;
let companyDeniedAmount;
let companyMean;
let companyMedian;
let companyOverallAmountRequested;
let companyRange;

// Money formatter for data within getCompanyStatistics function
let formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

// Gets company statistics
async function getCompanyStatistics() {
    let res = await fetch('http://localhost:5000/statistics')
    let dataRes;
    if (res.status === 202) {
        dataRes = await res.text()
        await dataRes
        console.log(dataRes);
    } else {
        dataRes = await res.json()
        await dataRes
        companyAcceptedAmount = dataRes._statistic_accepted_amount
        companyDeniedAmount = dataRes._statistic_denied_amount
        companyMean = dataRes._statistic_mean
        companyMedian = dataRes._statistic_median
        companyOverallAmountRequested = dataRes._statistic_overall_amount_request
        companyRange = dataRes._statistic_range
        let overall = formatter.format(companyOverallAmountRequested)
        let accepted = formatter.format(companyAcceptedAmount)
        let denied = formatter.format(companyDeniedAmount)
        let mean = formatter.format(companyMean)
        let median = formatter.format(companyMedian)
        let range = formatter.format(companyRange)
        document.getElementById('companyOAR').innerHTML += overall
        document.getElementById('companyOAA').innerHTML += accepted
        document.getElementById('companyODA').innerHTML += denied
        document.getElementById('companyMean').innerHTML += mean
        document.getElementById('companyMedian').innerHTML += median
        document.getElementById('companyRange').innerHTML += range
    }
}

// Gets all employees individual statistics
async function getAllEmployeesStatistics() {
    let res = await fetch('http://localhost:5000/employees')
    let data = await res.json()
    let count = 0
    let span = document.getElementById('modalContent')
    for (const employee in data) {
        let employeeName = data[employee]._employee_name
        let employeeORA = data[employee]._employee_statistics_list[0]._statistic_overall_amount_request
        let employeeOAA = data[employee]._employee_statistics_list[0]._statistic_accepted_amount
        let employeeODA = data[employee]._employee_statistics_list[0]._statistic_denied_amount
        let employeeMean = data[employee]._employee_statistics_list[0]._statistic_mean
        let employeeMedian = data[employee]._employee_statistics_list[0]._statistic_median
        let employeeRange = data[employee]._employee_statistics_list[0]._statistic_range
        let employeeRow = document.getElementById('employeeRow')
        let div = document.createElement('div')
        div.classList.add('card')
        div.classList.add('col-4')
        div.style.width = '25rem'
        let div2 = document.createElement('div')
        div2.classList.add('card-header')
        div2.style.cursor = 'pointer'
        let h4 = document.createElement('h4')
        h4.setAttribute('data-bs-toggle', 'modal')
        h4.setAttribute('data-bs-target', `#modal${count}`)
        h4.style.color = 'white'
        h4.innerHTML = employeeName
        div2.appendChild(h4)
        let unordered = document.createElement('ul')
        unordered.classList.add('list-group')
        unordered.classList.add('list-group-flush')
        let li1 = document.createElement('li')
        li1.classList.add('list-group-item')
        let li1Node = document.createTextNode(`Overall Amount Requested: ${employeeORA}`)
        li1.appendChild(li1Node)
        let li2 = document.createElement('li')
        li2.classList.add('list-group-item')
        let li2Node = document.createTextNode(`Overall Accepted Amount: ${employeeOAA}`)
        li2.appendChild(li2Node)
        let li3 = document.createElement('li')
        li3.classList.add('list-group-item')
        let li3Node = document.createTextNode(`Overall Denied Amount: ${employeeODA}`)
        li3.appendChild(li3Node)
        let li4 = document.createElement('li')
        li4.classList.add('list-group-item')
        let li4Node = document.createTextNode(`Mean Amount: ${employeeMean}`)
        li4.appendChild(li4Node)
        let li5 = document.createElement('li')
        li5.classList.add('list-group-item')
        let li5Node = document.createTextNode(`Median Amount: ${employeeMedian}`)
        li5.appendChild(li5Node)
        let li6 = document.createElement('li')
        li6.classList.add('list-group-item')
        let li6Node = document.createTextNode(`Range Amount: ${employeeRange}`)
        li6.appendChild(li6Node)
        unordered.appendChild(li1)
        unordered.appendChild(li2)
        unordered.appendChild(li3)
        unordered.appendChild(li4)
        unordered.appendChild(li5)
        unordered.appendChild(li6)
        div.appendChild(div2)
        div.appendChild(unordered)
        employeeRow.appendChild(div)
        // Modals for past request
        let modalDiv = document.createElement('div')
        modalDiv.classList.add('modal')
        modalDiv.classList.add('fade')
        modalDiv.setAttribute('aria-hidden', 'true')
        modalDiv.setAttribute('tabindex', '-1')
        modalDiv.setAttribute('id', `modal${count}`)
        let modalDiv1 = document.createElement('div')
        modalDiv1.classList.add('modal-dialog')
        let modalDiv2 = document.createElement('div')
        modalDiv2.classList.add('modal-content')
        let modalDiv3 = document.createElement('div')
        modalDiv3.classList.add('modal-header')
        let modalH4 = document.createElement('h4')
        modalH4.innerHTML = `${employeeName} - Request History`
        let xBtn = document.createElement('button')
        xBtn.setAttribute('type', 'button')
        xBtn.classList.add('btn-close')
        xBtn.setAttribute('data-bs-dismiss', 'modal')
        xBtn.setAttribute('aria-label', 'Close')
        modalDiv3.appendChild(modalH4)
        modalDiv3.appendChild(xBtn)
        modalDiv2.appendChild(modalDiv3)
        let modalDiv4 = document.createElement('div')
        modalDiv4.classList.add('modal-body')
        for(let i = 0; i < data[employee]._employee_reimbursements_list.length; i++) {
        if(data[employee]._employee_reimbursements_list.length > 0) {
            if(data[employee]._employee_reimbursements_list[i]._reimbursement_accepted !== 'Pending') {
                let requestStatus = data[employee]._employee_reimbursements_list[i]._reimbursement_accepted
                let requestAmount = data[employee]._employee_reimbursements_list[i]._reimbursement_amount
                let requestDate = data[employee]._employee_reimbursements_list[i]._reimbursement_date
                let requestReason = data[employee]._employee_reimbursements_list[i]._reimbursement_reason
                let requestNote = data[employee]._employee_reimbursements_list[i]._reimbursement_note
                let modalUl = document.createElement('ul')
                modalUl.classList.add('list-group')
                modalUl.classList.add('list-group-horizontal')
                let modalLi1 = document.createElement('li')
                modalLi1.classList.add('modal-list')
                modalLi1.classList.add('list-group-item')
                let li1TextNode = document.createTextNode(`Amount: ${requestAmount}`)
                modalLi1.appendChild(li1TextNode)
                let modalLi2 = document.createElement('li')
                modalLi2.classList.add('modal-list')
                modalLi2.classList.add('list-group-item')
                let li2TextNode = document.createTextNode(`Reason: ${requestReason}`)
                modalLi2.appendChild(li2TextNode)
                let modalLi3 = document.createElement('li')
                modalLi3.classList.add('modal-list')
                modalLi3.classList.add('list-group-item')
                let li3TextNode = document.createTextNode(`Date Requested: ${requestDate}`)
                modalLi3.appendChild(li3TextNode)
                let modalLi4 = document.createElement('li')
                modalLi4.classList.add('modal-list')
                modalLi4.classList.add('list-group-item')
                let li4TextNode = document.createTextNode(`Status: ${requestStatus}`)
                modalLi4.appendChild(li4TextNode)
                let modalLi5 = document.createElement('li')
                modalLi5.classList.add('modal-list')
                modalLi5.classList.add('list-group-item')
                let li5TextNode;
                if(requestNote === '' || requestNote === null) {
                    li5TextNode = document.createTextNode('Note: N/A')
                } else {
                    li5TextNode = document.createTextNode(`Note: ${requestNote}`)
                }
                modalLi5.appendChild(li5TextNode)
//                if(requestStatus === 'Denied') {
//                    modalLi4.style.color = 'red'
//                } else {
//                    modalLi4.style.color = 'green'
//                }
                modalUl.appendChild(modalLi1)
                modalUl.appendChild(modalLi2)
                modalUl.appendChild(modalLi3)
                modalUl.appendChild(modalLi4)
                modalUl.appendChild(modalLi5)
                modalDiv4.appendChild(modalUl)
            }
            }
        }
        modalDiv2.appendChild(modalDiv4)
        modalDiv1.appendChild(modalDiv2)
        modalDiv.appendChild(modalDiv1)
        span.appendChild(modalDiv)
        count++
    }
}

window.onload = function() {
    getCompanyStatistics()
    getAllEmployeesStatistics()
}
// Employee ID
let employeeId = localStorage.getItem('employeeId')
// Employee Data
let employeeData
// Employee Pending Amount
let pendingAmount
// Employee Past Amount
let pastAmount

// Submission of new request
let submitBtn = document.querySelector('#submitRequest')
submitBtn.addEventListener('click', async function() {
    let amountVal = Number(document.getElementById('amount').value)
    let reasonVal = document.getElementById('reason').value
    if(isNaN(amountVal)) {
        let invalid = document.getElementById('invalid')
        invalid.classList.remove('invisible')
        invalid.classList.add('visible')
        setTimeout(function() {
            invalid.classList.remove('visible')
            invalid.classList.add('invisible')
         }, 3000)
    } else {
        let data = { amount: amountVal, reason: reasonVal }
        let res = await fetch(`/employees/${employeeId}/reimbursements`, {
            method: 'POST',
            credentials: 'include',
            body: JSON.stringify(data),
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            })
        })
        let dataRes = await res.text()
        console.log(dataRes)
        if(dataRes === 'Successfully submitted reimbursement request') {
            let success = document.getElementById('success')
            success.classList.remove('invisible')
            success.classList.add('visible')
            setTimeout(function() {
                success.classList.remove('visible')
                success.classList.add('invisible')
                location.reload();
            }, 3000)
        } else {
            let invalid = document.getElementById('invalid')
            invalid.classList.remove('invisible')
            invalid.classList.add('visible')
        }
    }
})

// Gets employee info
async function getEmployeeData(id) {
    let res = await fetch(`http://localhost:5000/employees/${id}`)
    let dataRes;
    if(res.status === 404) {
        dataRes = await res.text()
        await dataRes
        console.log(dataRes)
    } else {
        dataRes = await res.json()
        await dataRes
        employeeData = dataRes
        console.log(employeeData)
        let fullName = document.getElementById('fullName')
        let employeeEmail = document.getElementById('employeeEmail')
        let textNodeName = document.createTextNode(`${employeeData[1]._employee_name}`)
        let textNodeEmail = document.createTextNode(`${employeeData[1]._employee_email}`)
        fullName.appendChild(textNodeName)
        employeeEmail.appendChild(textNodeEmail)
        let totalAmount = document.getElementById('employeeTRM')
        let textNodeTTRM = document.createTextNode(`${employeeData[1]._employee_reimbursements_list.length.toString()}`)
        totalAmount.appendChild(textNodeTTRM)
    }
}

// Gets pending amount
async function getPendingAmount(id) {
    let res = await fetch(`http://localhost:5000/employees/${id}/reimbursements?pending=true`)
    let dataRes;
    if(res.status === 202) {
        dataRes = await res.text()
        await dataRes
        console.log(dataRes)
    } else {
        dataRes = await res.json()
        await dataRes
        pendingAmount = dataRes.length
        console.log(pendingAmount)
        let pendingRequests = document.getElementById('pendingRequests')
        let textNode = document.createTextNode(`${pendingAmount.toString()}`)
        pendingRequests.appendChild(textNode)
    }
}

// Gets past amount
async function getPastAmount(id) {
    let res = await fetch(`http://localhost:5000/employees/${id}/reimbursements?past=true`)
    let dataRes;
    if(res.status === 202) {
        dataRes = await res.text()
        await dataRes
        console.log(dataRes)
    } else {
        dataRes = await res.json()
        await dataRes
        pastAmount = dataRes.length
        console.log(pastAmount)
        let resolvedRequests = document.getElementById('resolvedRequests')
        let textNodeResolved = document.createTextNode(`${pastAmount.toString()}`)
        resolvedRequests.appendChild(textNodeResolved)
    }
}



window.onload = function() {
    getEmployeeData(employeeId)
    getPendingAmount(employeeId)
    getPastAmount(employeeId)
}
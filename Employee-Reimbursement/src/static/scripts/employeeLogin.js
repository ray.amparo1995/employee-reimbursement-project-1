
let loginBtn = document.querySelector('#login-button')
loginBtn.addEventListener('click', async function (){
let emailVal = document.getElementById('floatingInput').value
let passwordVal = document.getElementById('floatingPassword').value
    let data = { email: emailVal, password: passwordVal }
    let res = await fetch('http://localhost:5000/employees/login', {
        method: 'POST',
        credentials: 'include',
        body: JSON.stringify(data),
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        })
    })
    let dataRes;
    if (res.status === 202) {
        dataRes = await res.text()
        await dataRes
        if (dataRes === 'Wrong credentials. Please log in again.') {
            let loginError = document.getElementById('loginError')
            loginError.classList.remove('invisible')
            loginError.classList.add('visible')
        }
    } else {
        dataRes = await res.json()
        await dataRes
        localStorage.setItem('employeeId', dataRes[1]._employee_id)
        window.location = 'http://localhost:5000/employee_dashboard.html'
    }
})

// Redirects to manager login page
let managerIcon = document.querySelector('#managerIcon')
managerIcon.addEventListener('click', function() {
    window.location = 'http://localhost:5000/manager_login.html'
})
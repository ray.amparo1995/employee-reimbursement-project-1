// Employee ID
let employeeId = localStorage.getItem('employeeId')
// Content Header
let headerStr = document.getElementById('contentHeader').innerHTML

async function getPendingRequests(id) {
    let res = await fetch(`http://localhost:5000/employees/${id}/reimbursements?pending=true`)
    let dataRes;
    if(res.status === 202) {
        dataRes = res.text()
        await dataRes
        console.log(dataRes)
    } else {
        dataRes = await res.json()
        await dataRes
        console.log(dataRes)
        headerStr = headerStr + ' ' + `(${dataRes.length})`
        console.log(headerStr)
        document.getElementById('contentHeader').innerHTML = headerStr
        for(let i = 0; i < dataRes.length; i++) {
            let span = document.getElementById('listItems')
            let unordered = document.createElement('ul')
            unordered.classList.add('list-group')
            unordered.classList.add('list-group-horizontal')
            let li1 = document.createElement('li')
            let li2 = document.createElement('li')
            let li3 = document.createElement('li')
            let li4 = document.createElement('li')
            li1.classList.add('list-group-item')
            li1.classList.add('pending-list')
            li2.classList.add('list-group-item')
            li2.classList.add('pending-list')
            li3.classList.add('list-group-item')
            li3.classList.add('pending-list')
            li4.classList.add('list-group-item')
            li4.classList.add('pending-list')
            let nodeli1 = document.createTextNode(`${dataRes[i]._reimbursement_amount}`)
            let nodeli2 = document.createTextNode(`${dataRes[i]._reimbursement_reason}`)
            let nodeli3 = document.createTextNode(`${dataRes[i]._reimbursement_date}`)
            let nodeli4 = document.createTextNode(`${dataRes[i]._reimbursement_accepted}`)
            li1.appendChild(nodeli1)
            li2.appendChild(nodeli2)
            li3.appendChild(nodeli3)
            li4.appendChild(nodeli4)
            unordered.appendChild(li1)
            unordered.appendChild(li2)
            unordered.appendChild(li3)
            unordered.appendChild(li4)
            span.appendChild(unordered)
        }
    }
}

window.onload = function() {
    getPendingRequests(employeeId)
}
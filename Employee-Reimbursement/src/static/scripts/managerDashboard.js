// Manager ID
let managerId = localStorage.getItem('managerId')

async function getRecentRequests() {
    let res = await fetch('http://localhost:5000/employees/reimbursements/recent')
    let dataRes;
    if(res.status === 202) {
        dataRes = await res.text()
        await dataRes
        console.log(dataRes)
    } else {
        dataRes = await res.json()
        await dataRes
        for(let i = 0; i < dataRes.length; i++) {
            let span = document.getElementById('table-list')
            let unordered = document.createElement('ul')
            unordered.classList.add('list-group')
            unordered.classList.add('list-group-horizontal')
            let li1 = document.createElement('li')
            li1.classList.add('list-group-item')
            li1.classList.add('pending-list')
            let li1TextNode = document.createTextNode(`${dataRes[i]._reimbursement_owner_name}`)
            li1.appendChild(li1TextNode)
            let li2 = document.createElement('li')
            li2.classList.add('list-group-item')
            li2.classList.add('pending-list')
            let li2TextNode = document.createTextNode(`${dataRes[i]._reimbursement_amount}`)
            li2.appendChild(li2TextNode)
            let li3 = document.createElement('li')
            li3.classList.add('list-group-item')
            li3.classList.add('pending-list')
            let li3TextNode = document.createTextNode(`${dataRes[i]._reimbursement_reason}`)
            li3.appendChild(li3TextNode)
            let li4 = document.createElement('li')
            li4.classList.add('list-group-item')
            li4.classList.add('pending-list')
            let li4TextNode = document.createTextNode(`${dataRes[i]._reimbursement_date}`)
            li4.appendChild(li4TextNode)
            unordered.appendChild(li1)
            unordered.appendChild(li2)
            unordered.appendChild(li3)
            unordered.appendChild(li4)
            span.appendChild(unordered)
        }
    }
}

async function getManagerInfo(id) {
    let res = await fetch(`http://localhost:5000/managers/${id}`)
    let dataRes;
    if(res.status == 202) {
        dataRes = await res.text()
        await dataRes
        console.log(dataRes)
    } else {
        dataRes = await res.json()
        await dataRes
        let managerName = document.getElementById('fullName')
        let managerEmail = document.getElementById('managerEmail')
        let fullName = document.createTextNode(`${dataRes['Manager']._manager_name}`)
        let email = document.createTextNode(`${dataRes['Manager']._manager_email}`)
        managerName.appendChild(fullName)
        managerEmail.appendChild(email)
    }
}

window.onload = function() {
    getRecentRequests()
    getManagerInfo(managerId)
}
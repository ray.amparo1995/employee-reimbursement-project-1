// Content Header
let headerStr = document.getElementById('contentHeader').innerHTML

async function getAllPendingRequests() {
    let res = await fetch('http://localhost:5000/employees?reimbursementsPending=true')
    let dataRes;
    if(res.status === 202) {
        dataRes = res.text()
        await dataRes
        console.log(dataRes)
    } else {
        dataRes = await res.json()
        await dataRes
        console.log(dataRes)
        headerStr = headerStr + ' ' + `(${dataRes.length})`
        console.log(headerStr)
        let count = 0;
        document.getElementById('contentHeader').innerHTML = headerStr
        for(let i = 0; i < dataRes.length; i++) {
            let span = document.getElementById('listItems')
            let unordered = document.createElement('ul')
            unordered.classList.add('list-group')
            unordered.classList.add('list-group-horizontal')
            let li1 = document.createElement('li')
            let li2 = document.createElement('li')
            let li3 = document.createElement('li')
            let li4 = document.createElement('li')
            let li5 = document.createElement('li')
            let review = document.createElement('button')
            review.setAttribute('data-bs-toggle', 'modal')
            review.setAttribute('data-bs-target', `#modal${count}`)
            review.setAttribute('id', `#modal${count}`)
            review.style.color = 'white'
            review.innerHTML = 'Review'
            review.setAttribute('type', 'btuton')
            review.classList.add('btn')
            review.classList.add('btn-warning')
            li1.classList.add('list-group-item')
            li1.classList.add('pending-list')
            li2.classList.add('list-group-item')
            li2.classList.add('pending-list')
            li3.classList.add('list-group-item')
            li3.classList.add('pending-list')
            li4.classList.add('list-group-item')
            li4.classList.add('pending-list')
            li5.classList.add('list-group-item')
            li5.classList.add('pending-list')
            let nodeli1 = document.createTextNode(`${dataRes[i]._reimbursement_owner_name}`)
            let nodeli2 = document.createTextNode(`${dataRes[i]._reimbursement_amount}`)
            let nodeli3 = document.createTextNode(`${dataRes[i]._reimbursement_reason}`)
            let nodeli4 = document.createTextNode(`${dataRes[i]._reimbursement_date}`)
            li1.appendChild(nodeli1)
            li2.appendChild(nodeli2)
            li3.appendChild(nodeli3)
            li4.appendChild(nodeli4)
            li5.appendChild(review)
            unordered.appendChild(li1)
            unordered.appendChild(li2)
            unordered.appendChild(li3)
            unordered.appendChild(li4)
            unordered.appendChild(li5)
            span.appendChild(unordered)
            // Modal For Request Response
            let modalSpan = document.getElementById('modalContent')
            let modalDiv = document.createElement('div')
            modalDiv.classList.add('modal')
            modalDiv.classList.add('fade')
            modalDiv.setAttribute('aria-hidden', 'true')
            modalDiv.setAttribute('tabindex', '-1')
            modalDiv.setAttribute('id', `modal${count}`)
            let modalDiv1 = document.createElement('div')
            modalDiv1.classList.add('modal-dialog')
            let modalDiv2 = document.createElement('div')
            modalDiv2.classList.add('modal-content')
            let modalDiv3 = document.createElement('div')
            modalDiv3.classList.add('modal-header')
            let modalH4 = document.createElement('h4')
            modalH4.classList.add('employeeName')
            modalH4.innerHTML = `${dataRes[i]._reimbursement_owner_name}'s Request`
            let xBtn = document.createElement('button')
            xBtn.setAttribute('type', 'button')
            xBtn.classList.add('btn-close')
            xBtn.setAttribute('data-bs-dismiss', 'modal')
            xBtn.setAttribute('aria-label', 'Close')
            modalDiv3.appendChild(modalH4)
            modalDiv3.appendChild(xBtn)
            modalDiv2.appendChild(modalDiv3)
            let modalDiv4 = document.createElement('div')
            modalDiv4.classList.add('modal-body')
            // Modal Body
            let modalUl = document.createElement('ul')
            modalUl.classList.add('list-group')
            let modalLi1 = document.createElement('li')
            modalLi1.classList.add('list-group-item')
            let modalLi1Node = document.createTextNode(`Employee: ${dataRes[i]._reimbursement_owner_name}`)
            modalLi1.appendChild(modalLi1Node)
            let modalLi2 = document.createElement('li')
            modalLi2.classList.add('list-group-item')
            let modalLi2Node = document.createTextNode(`Amount: ${dataRes[i]._reimbursement_amount}`)
            modalLi2.appendChild(modalLi2Node)
            let modalLi3 = document.createElement('li')
            modalLi3.classList.add('list-group-item')
            let modalLi3Node = document.createTextNode(`Reason: ${dataRes[i]._reimbursement_reason}`)
            modalLi3.appendChild(modalLi3Node)
            let modalLi4 = document.createElement('li')
            modalLi4.classList.add('list-group-item')
            let modalLi4Node = document.createTextNode(`Date Requested: ${dataRes[i]._reimbursement_date}`)
            modalLi4.appendChild(modalLi4Node)
            let noteHeader = document.createElement('h4')
            noteHeader.classList.add('noteHeader')
            noteHeader.innerHTML = 'Note (Optional)'
            let modalDivNote = document.createElement('div')
            modalDivNote.classList.add('input-group')
            let modalNote = document.createElement('textarea')
            modalNote.classList.add('form-control')
            modalNote.setAttribute('aria-label', 'With textarea')
            modalNote.setAttribute('id', 'optional-note')
            modalDivNote.appendChild(modalNote)
            let btnSpan = document.createElement('span')
            btnSpan.classList.add('btnSpan')
            let deny = document.createElement('button')
            let accept = document.createElement('button')
            deny.setAttribute('type', 'button')
            deny.setAttribute('id', `${dataRes[i]._reimbursement_id}`)
            deny.setAttribute('name', `${dataRes[i]._owner_id}`)
            deny.classList.add('btn')
            deny.classList.add('btn-danger')
            deny.innerHTML = 'DENY'
            deny.addEventListener('click', async function() {
                let optionalNote = modalNote.value
                let reimbursementId = deny.id
                let ownerId = deny.name
                let data = { denied: true, accepted: false, note: optionalNote }
                let res = await fetch(`http://localhost:5000/employees/${ownerId}/reimbursements/${reimbursementId}`, {
                    method: 'PATCH',
                    credentials: 'include',
                    body: JSON.stringify(data),
                    headers: new Headers({
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    })
                })
                let responseData = await res.text()
                await responseData
                if(responseData === 'Request has been denied') {
                    location.reload()
                }
            })
            accept.setAttribute('id', `${dataRes[i]._reimbursement_id}`)
            accept.setAttribute('name', `${dataRes[i]._owner_id}`)
            accept.setAttribute('type', 'button')
            accept.classList.add('btn')
            accept.classList.add('btn-success')
            accept.innerHTML = 'ACCEPT'
            accept.addEventListener('click', async function() {
                let optionalNote = modalNote.value
                let reimbursementId = accept.id
                let ownerId = accept.name
                let data = { accepted: true, denied: false, note: optionalNote }
                let res = await fetch(`http://localhost:5000/employees/${ownerId}/reimbursements/${reimbursementId}`, {
                    method: 'PATCH',
                    credentials: 'include',
                    body: JSON.stringify(data),
                    headers: new Headers({
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    })
                })
                let responseData = await res.text()
                await responseData
                if(responseData === 'Request has been accepted') {
                    location.reload()
                }
            })
            btnSpan.appendChild(deny)
            btnSpan.appendChild(accept)
            modalUl.appendChild(modalLi1)
            modalUl.appendChild(modalLi2)
            modalUl.appendChild(modalLi3)
            modalUl.appendChild(modalLi4)
            modalDiv4.appendChild(modalUl)
            modalDiv4.appendChild(noteHeader)
            modalDiv4.appendChild(modalDivNote)
            modalDiv4.appendChild(btnSpan)
            modalDiv2.appendChild(modalDiv4)
            modalDiv1.appendChild(modalDiv2)
            modalDiv.appendChild(modalDiv1)
            modalSpan.appendChild(modalDiv)
            count++
        }
    }
}


window.onload = function() {
    getAllPendingRequests()
}
import unittest
from unittest.mock import Mock, patch
from src.models.reimbursement_model import Reimbursement
import src.dao.reimbursement_dao as reimbursement_dao
import src.services.reimbursement_services as reimbursement_services
import src.dao.statistic_dao as statistic_dao
import src.dao.employee_dao as employee_dao

class ReimbursementServicesTest(unittest.TestCase):

    def setUp(self):
        reimbursement_dao.get_employee_reimbursement = Mock(return_value=[(2, 1, '$2,500.00', 'Business travel', '2021-05-23', False, 'Pending')])
        reimbursement_dao.get_past_reimbursements = Mock(return_value=[(3, 2, '$1,500.00', 'Computer', '2021-03-13', True, 'Accepted')])
        reimbursement_dao.get_pending_reimbursements = Mock(return_value=[(4, 5, '$500.00', 'Certifications', '2021-04-15', False, 'Pending')])
        self.mock_reimbursement_rows = [(2, 1, '$2,500.00', 'Business travel', '2021-05-23', False, 'Pending')]
        reimbursement_dao.create_reimbursement_request = Mock(return_value='Successfully submitted reimbursement request')
        statistic_dao.get_employee_statistics = Mock(return_value=[(7, '$2,500.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00')])
        statistic_dao.update_overall_requested_statistics = Mock(return_value='Successfully updated pending statistics (Overall requested amount)')
        reimbursement_dao.get_specific_reimbursement = Mock(return_value=[(8, 12, '$20,000.00', 'Rings bought, not earned', '2021-06-30', False, 'Pending')])
        reimbursement_dao.update_pending_request = Mock(return_value='Successfully updated reimbursement request')
        reimbursement_dao.delete_pending_request = Mock(return_value='Successfully deleted pending reimbursement request')
        reimbursement_dao.get_recent_requests = Mock(return_value=[(8, 14, 20000.00, 'Knee therapy', '2021-06-30', False, 'Pending')])
        employee_dao.get_employee_name = Mock(return_value=[['Kevin Durant']])
        reimbursement_dao.get_all_past_reimbursements = Mock(return_value=[(7, 10, 4000.00, 'Winning Rings', '2020-04-04', True, 'Accepted'), (7, 9, 10000.00, 'Winning MVP', '2021-04-10', True, 'Denied')])
        reimbursement_dao.get_all_pending_reimbursements = Mock(return_value=[(8, 14, 20000.00, 'Knee therapy', '2021-06-30', False, 'Pending'), (7, 13, 20000.00, 'Therapy', '2021-06-30', False, 'Pending')])
        reimbursement_dao.get_employee_denied_requests = Mock(return_value=[(4, 5, '$500.00', 'Certifications', '2021-04-15', True, 'Denied')])
        reimbursement_dao.deny_pending_request = Mock(return_value='Successfully denied request')
        statistic_dao.update_statistics_denied = Mock(return_value='Successfully updated employee statistic')
        reimbursement_dao.get_employee_accepted_requests = Mock(return_value=[(2, 1, '$2,500.00', 'Business travel', '2021-05-23', True, 'Accepted')])
        reimbursement_dao.accept_pending_request = Mock(return_value='Successfully accepted request')
        statistic_dao.update_statistics_accepted = Mock(return_value='Successfully updated employee statistic')

    def test_convert_reimbursement_rows(self):
        self.assertIsInstance(reimbursement_services.convert_reimbursement_rows(self.mock_reimbursement_rows), list)
        self.assertIsInstance(reimbursement_services.convert_reimbursement_rows(self.mock_reimbursement_rows)[0], Reimbursement)

    def test_get_employee_reimbursement(self):
        self.assertIsInstance(reimbursement_services.get_employee_reimbursement(2), list)
        self.assertIsInstance(reimbursement_services.get_employee_reimbursement(2)[0], Reimbursement)

    def test_get_past_reimbursements(self):
        self.assertIsInstance(reimbursement_services.get_past_reimbursements(3), list)
        self.assertIsInstance(reimbursement_services.get_past_reimbursements(3)[0], Reimbursement)

    def test_get_pending_reimbursements(self):
        self.assertIsInstance(reimbursement_services.get_pending_reimbursements(4), list)
        self.assertIsInstance(reimbursement_services.get_pending_reimbursements(4)[0], Reimbursement)

    def test_create_reimbursement_request(self):
        self.assertIsInstance(reimbursement_services.create_reimbursement_request(7, 500.00, 'MVP'), str)

    def test_update_pending_request(self):
        self.assertIsInstance(reimbursement_services.update_pending_request(8, 12, 1500.00, 'ACL Therapy'), str)

    def test_delete_pending_request(self):
        self.assertIsInstance(reimbursement_services.delete_pending_request(8, 12), str)

    def test_get_recent_requests(self):
        self.assertIsInstance(reimbursement_services.get_recent_requests(), list)
        self.assertIsInstance(reimbursement_services.get_recent_requests()[0], Reimbursement)

    def test_get_all_past_reimbursements(self):
        self.assertIsInstance(reimbursement_services.get_all_past_reimbursements(), list)
        self.assertIsInstance(reimbursement_services.get_all_past_reimbursements()[0], Reimbursement)

    def test_get_all_pending_reimbursements(self):
        self.assertIsInstance(reimbursement_services.get_all_pending_reimbursements(), list)
        self.assertIsInstance(reimbursement_services.get_all_pending_reimbursements()[0], Reimbursement)

    def test_deny_or_accept_request(self):
        self.assertIsInstance(reimbursement_services.deny_or_accept_request(2, 14, {"accepted": True, "denied": False}), str)
import unittest
from unittest.mock import Mock
import src.dao.statistic_dao as statistic_dao
import src.services.statistic_services as statistic_services
from src.models.statistics_model import Statistic
import src.dao.reimbursement_dao as reimbursement_dao

class StatisticServicesTest(unittest.TestCase):

    def setUp(self):
        statistic_dao.get_employee_statistics = Mock(return_value=[(2, '$2,500.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00')])
        self.mock_statistics_row = [(2, '$2,500.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00')]
        statistic_dao.create_statistics = Mock(return_value='Successfully created statistics for employee')
        statistic_dao.update_overall_requested_statistics = Mock(return_value='Successfully updated pending statistics (Overall requested amount)')
        reimbursement_dao.get_all_accepted_reimbursements = Mock(return_value=[(7, 10, '$4,000.00', 'Winning Rings', '2020-04-04', True, 'Accepted'),(3, 2, '$1,500.00', 'Computer', '2021-03-13', True, 'Accepted')])
        reimbursement_dao.get_all_denied_reimbursements = Mock(return_value=[(4, 5, '$500.00', 'Certifications', '2021-04-15', True, 'Denied'), (7, 9, '$10000.00', 'Winning MVP', '2021-04-10', True, 'Denied')])
        reimbursement_dao.get_all_reimbursement_requests = Mock(return_value=[(4, 5, '$500.00', 'Certifications', '2021-04-15', True, 'Denied'), (7, 9, '$10000.00', 'Winning MVP', '2021-04-10', True, 'Denied'), (7, 10, '$4,000.00', 'Winning Rings', '2020-04-04', True, 'Accepted'),(3, 2, '$1,500.00', 'Computer', '2021-03-13', True, 'Accepted')])

    def test_convert_statistics(self):
        self.assertIsInstance(statistic_services.convert_statistics(self.mock_statistics_row), list)
        self.assertIsInstance(statistic_services.convert_statistics(self.mock_statistics_row)[0], Statistic)

    def test_get_employee_statistics(self):
        self.assertIsInstance(statistic_services.get_employee_statistics(2), list)
        self.assertIsInstance(statistic_services.get_employee_statistics(2)[0], Statistic)

    def test_create_statistics(self):
        self.assertIsInstance(statistic_services.create_statistics(2, 200.00), str)

    def test_update_pending_statistics(self):
        self.assertIsInstance(statistic_services.update_pending_statistics(2, 1500.00), str)

    def test_get_company_statistics(self):
        self.assertIsInstance(statistic_services.get_company_statistics(), Statistic)
import unittest
from unittest.mock import Mock
import src.services.employee_services as employee_services
import src.dao.employee_dao as employee_dao
import src.dao.reimbursement_dao as reimbursement_dao
import src.dao.statistic_dao as statistic_dao
from src.models.reimbursement_model import Reimbursement
from src.models.statistics_model import Statistic


class EmployeeServicesTest(unittest.TestCase):

    def setUp(self):
        employee_dao.get_employee = Mock(return_value=[(2, 'lbj@lakers.com', 'iamking', 'Lebron James')])
        reimbursement_dao.get_employee_reimbursement = Mock(return_value=[(2, 1, '$2,500.00', 'Business travel', '2021-5-23', False, 'Pending')])
        statistic_dao.get_employee_statistics = Mock(return_value=[(2, '$2,500.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00')])
        employee_dao.get_all_employees = Mock(return_value=[(1, 'email@email.com', 'password', 'Kevin Durant'),(2, 'lbj@lakers.com', 'iamking', 'Lebron James')])

    # Get employee info data when they log in
    def test_get_employee(self):
        self.assertIsInstance(employee_services.get_employee(2), dict)
        self.assertIsInstance(employee_services.get_employee(2)['1']._employee_reimbursements_list[0], Reimbursement)
        self.assertIsInstance(employee_services.get_employee(2)['1']._employee_statistics_list[0], Statistic)

    # Gets all employees
    def test_get_all_employees(self):
        self.assertIsInstance(employee_services.get_all_employees(), dict)
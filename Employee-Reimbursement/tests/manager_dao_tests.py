from unittest.mock import patch
import unittest
import src.dao.manager_dao as manager_dao
import os
from dotenv import load_dotenv
from os.path import join, dirname

class ManagerDaoTest(unittest.TestCase):

    def setUp(self):
        from psycopg2 import connect, OperationalError
        dotenv_path = join(dirname(__file__), '.env')
        load_dotenv(dotenv_path)
        password = os.getenv('dev_password')
        self.con = connect(
            host='localhost',
            database='reimbursement_dev',
            user='raymondamparo',
            password=f'{password}'
        )

    @patch('src.dao.manager_dao.db_connection')
    def test_get_manager(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from managers where manager_id = 1')
        mock_query_row = mock_cur.fetchall()
        query_row = manager_dao.get_manager(1)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.manager_dao.db_connection')
    def test_get_manager_id(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select manager_id from managers where (manager_email = 'magic.johnson@lakers.com' and manager_password = 'bestpg123')")
        mock_query_row = mock_cur.fetchall()
        query_row = manager_dao.get_manager_id('magic.johnson@lakers.com', 'bestpg123')
        self.assertEqual(mock_query_row, query_row)
import unittest
from unittest.mock import Mock, patch
import src.services.manager_services as manager_services
import src.dao.manager_dao as manager_dao
from src.models.manager_model import Manager

class ManagerServicesTest(unittest.TestCase):

    def setUp(self):
        manager_dao.get_manager = Mock(return_value=[(1, 'Earvin Johnson', 'magic.johnson@lakers.com', 'bestpg123')])

    def test_get_manager(self):
        self.assertIsInstance(manager_services.get_manager(1), dict)
        self.assertIsInstance(manager_services.get_manager(1)['Manager'], Manager)
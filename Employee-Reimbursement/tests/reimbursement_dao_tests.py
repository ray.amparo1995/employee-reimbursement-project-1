from unittest.mock import patch
import unittest
import src.dao.reimbursement_dao as reimbursement_dao
import os
from dotenv import load_dotenv
from os.path import join, dirname

class ReimbursementDaoTest(unittest.TestCase):

    def setUp(self):
        from psycopg2 import connect, OperationalError
        dotenv_path = join(dirname(__file__), '.env')
        load_dotenv(dotenv_path)
        password = os.getenv('dev_password')
        self.con = connect(
            host='localhost',
            database='reimbursement_dev',
            user='raymondamparo',
            password=f'{password}'
        )

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_employee_reimbursement(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from reimbursements where owner_id = 7')
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_employee_reimbursement(7)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_past_reimbursements(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from reimbursements where (owner_id = 7 and reimbursement_resolved = true)')
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_past_reimbursements(7)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_pending_reimbursements(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from reimbursements where (owner_id = 7 and reimbursement_resolved = false)')
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_pending_reimbursements(7)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_specific_reimbursement(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from reimbursements where (owner_id = 7 and reimbursement_id = 37)')
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_specific_reimbursement(7, 37)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_recent_requests(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select * from reimbursements where (reimbursement_accepted = 'Pending' and reimbursement_resolved = false and reimbursement_date >= DATE(NOW()) + INTERVAL '-7 DAY' and reimbursement_date < DATE(NOW()) + INTERVAL '1 DAY')")
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_recent_requests()
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_all_past_reimbursements(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from reimbursements where reimbursement_resolved = true')
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_all_past_reimbursements()
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_all_pending_reimbursements(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select * from reimbursements where reimbursement_resolved = false and reimbursement_accepted = 'Pending'")
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_all_pending_reimbursements()
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_all_accepted_reimbursements(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select * from reimbursements where (reimbursement_resolved = true and reimbursement_accepted = 'Accepted')")
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_all_accepted_reimbursements()
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_employee_accepted_requests(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select * from reimbursements where (owner_id = 7 and reimbursement_resolved = true and reimbursement_accepted = 'Accepted')")
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_employee_accepted_requests(7)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_employee_denied_requests(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select * from reimbursements where (owner_id = 7 and reimbursement_resolved = true and reimbursement_accepted = 'Denied')")
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_employee_denied_requests(7)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_all_denied_reimbursements(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select * from reimbursements where (reimbursement_resolved = true and reimbursement_accepted = 'Denied')")
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_all_denied_reimbursements()
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.reimbursement_dao.db_connection')
    def test_get_all_reimbursement_requests(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select * from reimbursements")
        mock_query_row = mock_cur.fetchall()
        query_row = reimbursement_dao.get_all_reimbursement_requests()
        self.assertEqual(mock_query_row, query_row)
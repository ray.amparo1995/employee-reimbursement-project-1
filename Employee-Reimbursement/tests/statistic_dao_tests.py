from unittest.mock import patch
import unittest
import src.dao.statistic_dao as statistic_dao
import os
from dotenv import load_dotenv
from os.path import join, dirname

class StatisticDaoTest(unittest.TestCase):

    def setUp(self):
        from psycopg2 import connect, OperationalError
        dotenv_path = join(dirname(__file__), '.env')
        load_dotenv(dotenv_path)
        password = os.getenv('dev_password')
        self.con = connect(
            host='localhost',
            database='reimbursement_dev',
            user='raymondamparo',
            password=f'{password}'
        )

    @patch('src.dao.statistic_dao.db_connection')
    def test_get_employee_statistics(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from statistics where owner_id = 7')
        mock_query_row = mock_cur.fetchall()
        query_row = statistic_dao.get_employee_statistics(7)
        self.assertEqual(mock_query_row, query_row)
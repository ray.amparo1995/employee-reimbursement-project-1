from unittest.mock import patch
import unittest
import src.dao.employee_dao as employee_dao
import os
from dotenv import load_dotenv
from os.path import join, dirname

class EmployeeDaoTest(unittest.TestCase):

    def setUp(self):
        from psycopg2 import connect
        dotenv_path = join(dirname(__file__), '.env')
        load_dotenv(dotenv_path)
        password = os.getenv('dev_password')
        self.con = connect(
            host='localhost',
            database='reimbursement_dev',
            user='raymondamparo',
            password=f'{password}'
        )

    @patch('src.dao.employee_dao.db_connection')
    def test_get_employee(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute(f'select * from employees where employee_id = 7')
        mock_query_row = mock_cur.fetchall()
        query_row = employee_dao.get_employee(7)
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.employee_dao.db_connection')
    def test_get_all_employees(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select * from employees')
        mock_query_rows = mock_cur.fetchall()
        query_rows = employee_dao.get_all_employees()
        self.assertEqual(mock_query_rows, query_rows)

    @patch('src.dao.employee_dao.db_connection')
    def test_get_employee_id(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute("select employee_id from employees where (employee_email = 'dame@dolla.com' and employee_password = 'ineedaring')")
        mock_query_row = mock_cur.fetchall()
        query_row = employee_dao.get_employee_id('dame@dolla.com', 'ineedaring')
        self.assertEqual(mock_query_row, query_row)

    @patch('src.dao.employee_dao.db_connection')
    def test_get_employee_name(self, mock_con):
        mock_con.return_value = self.con
        mock_cur = self.con.cursor()
        mock_cur.execute('select employee_name from employees where employee_id = 7')
        mock_query_row = mock_cur.fetchall()
        query_row = employee_dao.get_employee_name(7)
        self.assertEqual(mock_query_row, query_row)
# Employee-Reimbursement-Project 1

## Project Description
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can log in and submit requests for reimbursements and view their past tickets and pending requests. Managers can log in and view all reimbursement requests and past history for all employees in the company. Managers are authorized to approve and deny requests for expense reimbursements.

## Technologies Used

- JavaScript

- HTML

- CSS

- SQL

- Python

- Flask

- Postman

- AWS RDS

- psycopg2

- Bootstrap

## Features

List of features ready

- Employee Login

- Employee Dashboard

- Employee Request Submission

- Employee Pending Requests

- Employee Past Requests

- Manager Login

- Manager Dashboard

- Manager Pending Request Review

- Company and Employee Statistics

## Getting Started

1. Clone repo to local machine utilizing HTTPs via Version Control in PyCharm

2. Configure run type and Python interpreter

3. pip install flask, flask-cors, psycopg2, python-dotenv

4. Configure local database utilizing sql script within the sql folder of this repo located within the employee-reimbursement folder

## Usage

1. Run main.py and access http://localhost:5000/employee_login.html to access the employee dashboard

2. Run main.py and access http://localhost:5000/manager_login.html to access the manager dashboard 
